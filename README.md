# remms-web

Website for RemMS - MapleStory private server.

## Introduction

This repository contains the web files for RemMS. This uncludes backend, as well as front end.

Story about MapleStory and the private server bla bla...

## Functionality

The functionality is split into two parts. A front end and a  back end.
The front end should be tightly connected to the backend in order to fulfill their purpose.

## Front end

The purpose of the front end is to provide players a place to do the following.
* Create an account
* A `vote for nx` button
* A link to download the files
* (optional) a link to donate
* (optional) login page, so users can check their balance etc


## Back end

The backend should provide the front end with corresponding functionality. It should also host the website. The back end should be connected to the database in order to work. 

The back end will be responsible for the following:
* Host the web page(s)
* Handle `Create account` functionality
* Check for votes on [gtop100]("https://gtop100.com/topsites/MapleStory"), and assign NX to accounts
* (optional) assign NX for donations
* (optional) retrieve account info
