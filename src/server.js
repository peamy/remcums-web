const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const db = require('./database/dbconnection');
const jobs = require('./scheduled-jobs/jobs');

var app = express();
const connection = db.createMysqlConnection();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
const publicPath = path.join(__dirname, '../public/');
app.use(express.static(publicPath));

app.get('/', (req, res) => {
  console.log('Got a request on GET /');
  res.sendFile(publicPath + 'index.html');
});

app.post('/create', (req, res) => {
  console.log('Got a request on POST /create');
    db.createAccount(req.body.email, req.body.username, req.body.password, connection);
  res.send({"status": "succes!"});
});

app.listen(3000, () => {
    console.log('App up and running, listening on port 3000');    

    jobs.checkIfLoggedIn()
});
