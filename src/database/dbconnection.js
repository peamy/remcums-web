const mysql = require('mysql');

const config = require('../../config.json');

const createMysqlConnection = () => {
	var connection = mysql.createConnection({
		host     : 'localhost',
		user     : 'root',
		password :  config.database.password,
		database : 'heavenms'
	});
	return connection
};

function createAccount(email, user, pw, connection) {	
	connection.connect()
	var querystring = `INSERT INTO \`heavenms\`.\`accounts\` (\`name\`, \`password\`, \`banned\`, \`nxCredit\`, \`maplePoint\`, \`nxPrepaid\`, \`characterslots\`, \`webadmin\`, \`email\`, \`rewardpoints\`, \`votepoints\`) VALUES ('${user}', '${pw}', '0', '0', '0', '0', '3', '0', '${email}', '0', '0');`
	try {
		connection.query(querystring, function (error, results, fields) {
			if (error) throw error;
			// console.log('The solution is: ', results[0].solution);
		});
	}
	catch(error) {
		console.log('caught exception')
	}

	connection.end();
}

module.exports = {
	createMysqlConnection,
	createAccount
};
